# Dockerize Notification Microservice

This tutorial is to create an isolated system for Notification in Kudo


### Usage

We are going to dockerize 3 repository that related to this project.

Kudo API Seller 
```
git clone https://bitbucket.org/kudoindonesia/kudo_api_seller
```
Kudo seller cms
```
git clone https://bitbucket.org/kudoindonesia/kudo_seller_cms
```
Notification Microservice

```
git clone https://bitbucket.org/asepsaepulloh/microservice_notification.git
```

### Dockerize Kudo API Seller

First of all, clone the repository 
```
git clone https://bitbucket.org/kudoindonesia/kudo_api_seller
```

then, follow the instruction to [get started](https://bitbucket.org/kudoindonesia/kudo_api_seller/src/839167aec3a19dcfee9517bf463055cb3a348e45/README.md?at=master&fileviewer=file-view-default) with API 

Now, it's time to **Dockerize**

Make sure that you already installed docker, if you have not installed that, follow [this](https://docs.docker.com/engine/installation/) instructions.

run this command to test that your docker is correctly installed
```
docker version
```

`important`
_If permission is denied , try using sudo for each docker command_

Next, we're going to pull an images that related to our repository, for this cases we're using python 2.7.12 for API

```
docker pull python:2.7.12
```

check your images, if it's already installed. It have to be listed in

```
docker images
```
Create container
```
sudo docker run --rm -p 5000:8001 -v /var/www/html/kudo_api_seller/:/var/www/html/kudo_api_seller -it python:2.7.12 /bin/bash
```

from the command above your container will be served in _http://localhost:5000_ and you can find *kudo_api_seller* directory on /var/www/html/ on your container folder

`important`
_--rm is for temporary container_

now, for run server, you have to create a virtual env, in this way, I created virtual env that called myenv on /var/www/html/myenv directory

```
virtualenv myenv
```

activate environement, make sure that you are on myenv directory

```
source bin/activate
```

install all the requirements and make sure you are in the same directory with your requirements.txt

```
pip install --upgrade -r requirements.txt
```

and run your server

```
python run.py
```

Gotcha!! Your program already served in [port:5000](https://localhost:5000)


### Dockerize Kudo CMS Seller

Clone the repository 
```
git clone https://bitbucket.org/kudoindonesia/kudo_seller_cms
```

then, follow the instruction to [get started] (https://bitbucket.org/kudoindonesia/kudo_seller_cms) with CMS Seller

**Dockerize Time**

like in the tutorial above we have to pull an images, and for kudo_seller_cms we're using 

```
docker pull python:3.5.4
```

Create container
```
sudo docker run --rm -p 5001:8815 -v ~/Documents/Internship/Kudo/kudo_seller_cms:/var/www/html/kudo_seller_cms -it python:3.5.4 /bin/bash
```

from the command above your container will be served in _http://localhost:5001_ and you can find *kudo_seller_cms* directory on /var/www/html/ on your container folder

`important`
_--rm is for temporary container_

now, for run server, you have to create a virtual env, in this way, I created virtual env that called myenv on /var/www/html/myenv directory

```
virtualenv myenv
```
`important`
_if error install the virtualenv first with pip3 install virtualenv_


activate environement, make sure that you are on myenv directory

```
source bin/activate
```

install all the requirements and make sure you are in the same directory with your requirements.txt

```
pip3 install --upgrade -r requirements.txt
```

and run your server

```
python3 run.py
```

now, you can acces your server in http://localhost:5001

### Dockerize Microservice Notification

Clone the repository 
```
git clone https://bitbucket.org/asepsaepulloh/microservice_notification.git
```

then, follow the instruction to [get started](https://bitbucket.org/asepsaepulloh/microservice_notification) with microservice

**Dockerize Time**

like in the tutorial above we have to pull an images, and for this case we use node:6.11.2 

```
docker pull node:6.11.2
```

Create container
```
sudo docker run --rm -p 5002:3000 -v ~/Documents/Internship/Kudo/microservice_notification:/var/www/html/microservice_notification -it node:6.11.1 /bin/bash
```

from the command above your container will be served in _http://localhost:5002_ and you can find *notification_microservice* directory on /var/www/html/ on your container folder

`important`
_--rm is for temporary container_

run your server

```
node server.js
```

now, you can acces your server in http://localhost:5001

**update**
add --net="host" on your sudo docker run if you have a problem with port

## Authors

* **Nur intan Alatas**- [Github Repository](https://github.com/Nurintaaan)


## License

This project is licensed under the MIT License
